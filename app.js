var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var ctx = canvas.getContext("2d");

var mouse = {
  x: undefined,
  y:undefined
}
// adding event listener when mouse cursor is moving on the window
window.addEventListener("mousemove",function(event){
  // extracting mouse position coordinates
  mouse.x = event.x;
  mouse.y = event.y;
});

// resizing window make the circles not parted evenly
window.addEventListener("resize",function(){
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  init();
});
// array of colors of our Circles
var colors = [
  '#bc986a',
  '#daad86',
  '#fbeec1',
  '#659dbd',
  '#afd275'
];

var maxRadius = 60;
// var minRadius = 10;

function circle(x,y,radius,dx,dy){
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.dx = dx;
  this.dy = dy;
  this.minRadius = radius;
  this.color = colors[Math.floor(Math.random()*colors.length)];

  this.draw = function(){
    //ctx.clearRect();
    ctx.beginPath();
    ctx.arc(this.x,this.y,this.radius,0,Math.PI*2,true);
    ctx.fillStyle = this.color;
    ctx.fill();
  }

  this.update = function(){
    if (this.x + this.radius > window.innerWidth || this.x -this.radius < 0) {
      this.dx = -this.dx;
    }else if (this.y + this.radius > window.innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.dy += this.dy;

    // add interactivity conditions
    // increase circles size within mouse interval -50,50
    if (mouse.x - this.x < 50  && mouse.y - this.y <50 && mouse.x - this.x > -50  && mouse.y - this.y > -50){
      this.radius += 1;
      if (this.radius > maxRadius) {
        // shrink circles size if they have a radius > max
        this.radius = maxRadius;
      }
    }else if(this.radius > this.minRadius){
      this.radius -= 1;
    }
    this.draw();
  }
}


// array of circles
var Circles = [];

// fill array
for (var i = 0; i < 1000; i++) {
  var x = Math.random()*window.innerWidth;
  var y = Math.random()*window.innerHeight;
  // variable circle size from 1 to 4 for nicer view
  var radius = Math.random()*3 + 1;
  var dx = Math.random()*5;
  var dy = Math.random()*5;
  Circles.push(new circle(x,y,radius,dx,dy));
}

// to fill the empty screen corners with circles when window is resized
function init(){
  Circles = [];
  // fill array
  for (var i = 0; i < 1000; i++) {
    var x = Math.random()*window.innerWidth;
    var y = Math.random()*window.innerHeight;
    // variable circle size from 1 to 4 for nicer view
    var radius = Math.random()*3 + 1;
    var dx = Math.random()*5;
    var dy = Math.random()*5;
    Circles.push(new circle(x,y,radius,dx,dy));
  }
}

function animate(){
  requestAnimationFrame(animate);
  ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
  for (var i = 0; i < Circles.length; i++) {
    Circles[i].update();
  }

}

init();
animate();
